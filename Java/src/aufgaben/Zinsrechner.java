package aufgaben;

import java.util.Scanner;

public class Zinsrechner {

	public static void main(String[] args) {
		// Programmieren Sie einen Zinsrechner der zu einem Startkapital, einem Zinssatz und einer Laufzeit das Endkapital berechnet.
		// Beispieldaten: Startkapital: 1000 Euro, Zinssatz: 5%, Laufzeit: 5 Jahre => Endkapital: 1276,28 Euro
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie das Startkapital in Euro ein.");
		int startkapital = sc.nextInt();
		System.out.println("Bitte geben Sie den Zinssatz in Prozent ein.");
		double zinssatz = sc.nextDouble();
		System.out.println("Bitte geben Sie das Laufzeit in Jahren ein.");
		int laufzeit = sc.nextInt();
		
		double endkapital = startkapital * Math.pow(1 + zinssatz / 100, laufzeit);
		
		// System.out.println("Endkapital = " + endkapital + " Euro");
		System.out.printf("Ihr Endkapital betr�gt %,.2f Euro%n", endkapital);
		sc.close();
	}

}
