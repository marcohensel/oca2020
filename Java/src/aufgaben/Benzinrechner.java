package aufgaben;

import java.util.Scanner;

public class Benzinrechner {

	public static void main(String[] args) {
		// Programmieren Sie einen Benzinrechner, der zu dem Gesamtverbrauch in Liter und den gefahrenen Kilometern den Verbrauch auf 100 Kilometer 
		// berechnet. Beispieldaten: 60 Liter, 400 Kilometer = 15 Liter / 100 Kilometer
		Scanner sc = new Scanner(System.in);
		// Deklaration der Variablen und Zuweisung der Werte
//		int verbrauchGesamt = 31;
		System.out.println("Bitte geben Sie den Gesamtverbrauch ein.");
		int verbrauchGesamt = sc.nextInt();
//		int gefahreneKilometer = 407;
		System.out.println("Bitte geben Sie die gefahrenen Kilometer ein.");
		int gefahreneKilometer = sc.nextInt();
		double verbrauch100;
		
		// Berechnung des Verbrauchs auf 100 Kilometer
		verbrauch100 = verbrauchGesamt * 100.0 / gefahreneKilometer;
		
		// Ausgabe des Verbrauchs auf 100 Kilometer
		System.out.println("Verbrauch auf 100 Kilometer = " + verbrauch100 + " Liter");
		// Formatierte Ausgabe mit 1 Nachkommastelle %.1f und Zeilenumbruch %n
		System.out.printf("Verbrauch auf 100 Kilometer = %.1f Liter%n", verbrauch100);
		System.out.format("Sie sind %d Kilometer gefahren. Sie haben %d Liter verbraucht. Ihr Verbrauch auf 100 Kilometer betr�gt %.1f Liter.%n", 
				gefahreneKilometer, verbrauchGesamt, verbrauch100);
		// Schlie�en des Scanners
		sc.close();
		
	}

}
